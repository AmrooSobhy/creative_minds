//
//  ViewController.swift
//  creative_minds
//
//  Created by lapstore on 9/27/19.
//  Copyright © 2019 AmrSobhy. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
     let defaultSession = URLSession(configuration: .default)
     var dataTask: URLSessionDataTask?
    @IBOutlet weak var collectionView: UICollectionView!
    // amr zayed
    // amr zayed
    // amr zayed
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        var url = "https://realestate.cems-it.com/api/v1/cities"
        self.get(url: url, complete: { (json) in
            print("amrsobhy",json)
        }) { (error) in
            print("error2",error)
        }
        
        
        NetworkClient.performRequest([ToDoModel].self, router: APIRouter.getTodoLists, success: { [weak self] (models) in
            self?.data = models
            self?.myTableView.reloadData()
        }) { [weak self] (error) in
            self?.displayError(error.localizedDescription)
            
        }
    }


    // MARK:- UICollectionView methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        if section == 1{
            return 5
        }
        if section == 2{
            return 2
        }
        if section == 3{
            return 5
        }
        if section == 4{
            return 5
        }

        return Int()
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as? CollectionReusableView{
            
            
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            return CGSize()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
     func get(url: String,complete: @escaping (_: [String:Any])->(), NoInternet: @escaping (_: String)->()) {
        dataTask?.cancel()
        
        let url = URL(string: url)
        
        print("Printing GET URL \(String(describing: url))")
        
        dataTask = defaultSession.dataTask(with:url!) { (data, response, error) in
            
            guard error == nil else {
                print("NetworkManager - Networking/Request Error:", error!.localizedDescription)
                if error!.localizedDescription == "The Internet connection appears to be offline."{
                    print("amrsobhy")
                    NoInternet((error?.localizedDescription)!)
                }
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                print("data",data,"response",response,"error",error)
                let json1 = try JSONSerialization.jsonObject(with: data)
                print("json",json1)
                if let json = try JSONSerialization.jsonObject(with: data) as? [String:Any] {
                    
                    if let error = json["error"] as? Dictionary<String,Any>{
                        print(error)
                        
                    }
                    complete(json)
                }
            } catch let error as NSError {
                print("NetworkManager - JSON Parsing Error:", error.localizedDescription)
            }
        }
        
        
        dataTask?.resume()
    }
}

