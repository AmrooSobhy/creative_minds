//
//  CollectionReusableView.swift
//  creative_minds
//
//  Created by lapstore on 9/27/19.
//  Copyright © 2019 AmrSobhy. All rights reserved.
//

import UIKit

class CollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var label: UILabel!

}
